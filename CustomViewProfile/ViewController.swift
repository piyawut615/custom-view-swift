//
//  ViewController.swift
//  InspectableAndDesignable
//
//  Created by Piyawut Kamwiset on 11/26/2560 BE.
//  Copyright © 2560 Piyawut Kamwiset. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var profileView: ProfileView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileView.imageProfile.image = #imageLiteral(resourceName: "profile")
        profileView.lblName.text = "Piyawut Kamwiset"
        profileView.lblPhoneNumber.text = "XXX-XXX-XXXX"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

