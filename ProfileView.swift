//
//  ProfileView.swift
//  InspectableAndDesignable
//
//  Created by Piyawut Kamwiset on 11/26/2560 BE.
//  Copyright © 2560 Piyawut Kamwiset. All rights reserved.
//

import UIKit

class ProfileView: UIView {

    @IBOutlet var contentView: UIView! //
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // จะเข้าตอน init ใน csde
        loadViewFromNib()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // จะเข้าตอนโยง outlet ใน interface builder
        loadViewFromNib()
    }
    
    
    private func loadViewFromNib()  {
        let bundle = Bundle(for: ProfileView.self)
        bundle.loadNibNamed(String(describing: ProfileView.self), owner: self, options: nil)
        addSubview( contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }

}
